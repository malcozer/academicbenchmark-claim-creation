DO $$
<<edfi_block>>
DECLARE
    action_id integer;
    action_name varchar;
    application_id integer;
    application_name varchar := 'Ed-Fi ODS API';
    claim_set_id integer;
    claim_set_name varchar := 'AB Vendor';
BEGIN

    -- retrieve application id
    SELECT applications.applicationid INTO application_id FROM dbo.applications WHERE applications.applicationname = application_name;

    RAISE NOTICE 'Application ID % selected.', application_id;

    -- Ensuring AB Vendor Claimset exists.
    INSERT INTO dbo.claimsets (claimsetname, application_applicationid) VALUES (claim_set_name, application_id);

    -- Configure AB Vendor ClaimSet
    DROP TABLE IF EXISTS resourcenames;
    CREATE TEMP TABLE resourcenames (
       resourcename varchar
    );

    DROP TABLE IF EXISTS resourceclaimids;
    CREATE TEMP TABLE resourceclaimids (
       resourceclaimid integer
    );

    -- create temporary records.
    INSERT INTO resourcenames VALUES ('gradeLevelDescriptor'),('academicSubjectDescriptor'),('publicationStatusDescriptor'),('educationStandards');
    INSERT INTO resourceclaimids SELECT resourceclaimid FROM dbo.resourceclaims WHERE resourcename IN (SELECT resourcename FROM resourcenames)

    SELECT actions.actionid INTO action_id FROM dbo.actions WHERE actions.actionname = action_name;
    SELECT claimsets.claimsetid INTO claim_set_id FROM dbo.claimsets WHERE claimsets.claimsetname = claim_set_name;

    -- Configuring Claims for AB Vendor Claimset...
    
    -- incomplete
    INSERT INTO dbo.claimsetresourceclaims
        (action_actionid, claimset_claimsetid, resourceclaim_resourceclaimid)
        SELECT
               actionid, claim_set_id, resourceclaimid
        FROM dbo.actions
        CROSS JOIN resourceclaimids
        WHERE NOT EXISTS (
            SELECT *
            FROM dbo.claimsetresourceclaims
            WHERE
                claimsetresourceclaims.action_actionid = actions.actionid
                AND ClaimSetResourceClaims.claimset_claimsetid = claim_set_id
                AND resourceclaimids.resourceclaimid
        );

END edfi_block $$;
